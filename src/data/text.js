export default  [
    {
        id:"1",
        question:'Какая компания разработала React JS?',
        variants:['Twitter', 'Google', 'Amazon', 'GitHub','Facebook'],
        answer:'Facebook'
    },
    {
        id:"2",
        question:'Что необходимо прописать в теге script чтобы JSX корректно обрабатывался?',
        variants:['babel', 'text/javascript','text/babel','text/jsx','babel/text'],
        answer:'text/babel'
    },
    {
        id:"3",
        question:'Можно ли писать не используя Babel?',
        variants:['Да, можно', 'Нет, нельзя'],
        answer:'Да, можно'
    },
    {
        id:"4",
        question:'Где правильно передена функция в качестве свойства?',
        variants:['argument={this.someFunction}', 'argument={this.someFunction {}}','argument={someFunction}','argument=(this.someFunction)','argument={this.someFunction ()}'],
        answer:'argument={this.someFunction}'
    },
    {
        id:"5",
        question:'Как обратится к свойству weight?',
        variants:['{props.weight}', '{this.props.weight}', '{this.prop.weight}','{prop.weight}','{this.weight}'],
        answer:'{this.props.weight}'
    },
    {
        id:"6",
        question:'Чем свойства отличаются от состояний?',
        variants:['Свойства можно изменять, состояния нельзя', 'Свойства для работы со значениями, состояния для работы с функциями', 'Состояния для работы со значениями, свойства для работы с функциями','Состояния можно изменять, свойства нельзя'],
        answer:'Состояния можно изменять, свойства нельзя'
    },
    {
        id:"7",
        question:'React JS это...',
        variants:['MVC-фреймворк', 'JS библиотека', 'фреймворк'],
        answer:'JS библиотека'
    },
    {
        id:"8",
        question:'Куда можно встроить готовый код из метода рендера?',
        variants:['Только в тег, у которого есть id', 'В div или же в span', 'В любой тег','Только в div'],
        answer:'В любой тег'
    },
    {
        id:"9",
        question:'Сколько родительских элементов можно вывести одновременно?',
        variants:['Не более 3', 'Не более 5', 'Не более 10','Неограниченное количество','Не более 1'],
        answer:'Не более 1'
    },
    {
        id:"10",
        question:'Как много компонентов может быть на сайте?',
        variants:['Не более 10', 'Не более 100', 'Не более 50', 'Не более 300','Неограниченное количество'],
        answer:'Неограниченное количество'
    },
]
