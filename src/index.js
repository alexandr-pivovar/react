import ReactDOM from 'react-dom';
import React from 'react';
import MainComponent from './js/mainComponent/mainComponent.jsx';

ReactDOM.render(
    <MainComponent />,
    document.getElementById('root')
);
