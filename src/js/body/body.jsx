import React from 'react';
import CustomButton from '../buttons/custom';
import './body.css';
import Data from '../../data/text';
import Question from '../question/question';
import StatusPanel from '../status/statusPanel';

export default class Body extends React.Component {
    state = {
        data: Data,
        indexator: 0,
        result: Array(Data.length).fill({ answ: '' })
    };

    render() {
        const { data, indexator, result } = this.state;
        window.state = this.state;

        return (
            <React.Fragment>
                <div className="wrapper-status-list">
                    <StatusPanel
                        result={result}
                        callback={this.statusCallBack}
                    />
                </div>

                <div className='wrapper'>
                    <div className='left-button'>
                        <CustomButton
                            callback={this.Back}
                            name='back'
                        />
                    </div>
                    <div className='wrapper-for-main-question'>
                        <Question
                            callback={this.GetResult}
                            data={data[indexator]}
                            result={result[indexator]
                            }
                        />
                    </div>
                    <div className='right-button'><CustomButton callback={this.Next} name='next' /></div>
                </div>

                <div>
                    <CustomButton name='Get result' callback={this.Finish} />
                </div>
            </React.Fragment>
        );
    };

    statusCallBack = (index) => {
        index = parseInt(index);
        this.setState(() => ({ indexator: index }));
    };

    GetResult = (value) => {
        const index = this.state.indexator;
        const tempResult = this.state.result.slice();

        if (this.state.data[index].answer === value) {
            tempResult[index] = { answ: 'good' };
            this.setState(() => ({ result: tempResult }));
        } else {
            tempResult[index] = { answ: 'wrong' };
            this.setState(() => ({ result: tempResult }));
        }
    };

    Next = () => {
        const index = this.state.indexator;
        const length = this.state.data.length;

        if (index + 1 === length) {
            this.setState(() => ({ indexator: 0 }));
            return;
        }

        this.setState(() => ({ indexator: (index + 1) }));
    };

    Back = () => {
        const index = this.state.indexator;
        const length = this.state.data.length;

        if (index - 1 < 0) {
            this.setState(() => ({ indexator: length - 1 }));
            return;
        }

        this.setState(() => ({ indexator: (index - 1) }));
    };

    Finish = () => {
        const result = this.state.result.slice();
        var res = 0;

        for (let i = 0; i < result.length; i++) {
            if (result[i].answ === '') {
                alert('Вы ответели не на все вопросы!');
                return;
            }
        }

        for (let i = 0; i < result.length; i++) {
            if (result[i].answ === 'good') {
                res = res + 1;
            }
        }

        res = res / result.length * 100;

        alert('Ваш результат: ' + res.toPrecision(2).toString() + '%');
    };
};