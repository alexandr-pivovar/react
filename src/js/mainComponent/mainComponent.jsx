import React from 'react';
import './mainComponent.css';
import CustomButton from '../buttons/custom'
import Body from '../body/body'

export default class MainComponent extends React.Component {

    state = {
        isLogged: false,
    };

    LoggedIn = () => {
        this.setState(state => ({ isLogged: !state.isLogged }));
    };

    render() {

        const { isLogged } = this.state;

        return (
            <div className="main">
                <div className="header">
                    <CustomButton name={isLogged ? 'Log out' : 'Log in'} callback={this.LoggedIn} />
                    {isLogged && <CustomButton name='Settings' />}
                </div>
                <div className="body"><Body /></div>
            </div>
        );
    };
};