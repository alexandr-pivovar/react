import React from 'react';
import './question.css';

export default class Question extends React.Component {

    handleChange = e => {
        e.preventDefault();
        this.props.callback(e.target.value);
    };

    render() {
        const { result, data } = this.props;

        const buttons = data.variants.map(v =>
            <div key={v.toString()} className="question-buttons">
                <button type="button" value={v} onClick={this.handleChange}>{v}</button>
            </div>
        );

        return (
            <React.Fragment>
                <div className="question-text">{data.question}</div>
                {result.answ === '' ? buttons : null}
            </React.Fragment>
        );
    };
};