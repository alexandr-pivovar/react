import React from 'react';

export default class Status extends React.Component {

    changeIndex = (e) => {
        e.preventDefault();
        this.props.callback(e.target.value);
    };

    render() {
        const { name, value, backgroundColor } = this.props;

        return (
            <button
                className="wrapper-status-button"
                type="button"
                style={{ background: backgroundColor }}
                value={value}
                onClick={this.changeIndex}>{name + 1}
            </button>
        );
    };
};
