import React from 'react';
import Status from './status';
import './statusPanel.css';
import PropTypes from 'prop-types';

const StatusPanel = props => {
    const {
        result,
        callback,
    } = props;

    const statusList = result.map((currElement, index) =>
        <div key={index} className="wrapper-status">
            <Status
                name={index}
                value={index}
                callback={callback}
                backgroundColor={currElement.answ === '' ? 'grey' : 'green'}
            />
        </div>
    );

    return (
        <div className="statusList">
            {statusList}
        </div>
    );
};

StatusPanel.PropTypes = {
    result: PropTypes.array,
    callback: PropTypes.func,
};

export default StatusPanel;