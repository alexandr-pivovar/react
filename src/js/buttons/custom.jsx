import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import PropTypes from 'prop-types';

const CustomButton = props => {
    const {
        name,
        callback,
    } = props;

    return (
           <button type="button" className="btn btn-success" onClick={callback && callback}>{name}</button>

    );
};

CustomButton.propTypes = {
    callback: PropTypes.func.isRequired,
    name: PropTypes.string,
};

CustomButton.defaultProps = {
    name: 'Custom Button',
};

export default CustomButton;